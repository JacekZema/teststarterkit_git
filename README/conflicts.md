# Conflicts

> you have unmerged paths.

## Focus

The conflicts occur when we want to edit file which someone has changed the same line of a file or when file was deleted and these changes are pushed on remote before us. The conflicts occur after merge operation.

## Steps

1.  identify files containing conflicts
2.  open files in favourite text editor
3.  identify differences between HEAD and branch
4.  decide what to keep (HEAD, branch of combination of them)
5.  save changes
6.  you can commit changes in normal mode (add,commit)

repeat these steps for all conflicted files

## Example

Example, type `git status` when you have conflicts:

```
# On branch feature/feature1
# You have unmerged paths.
#   (fix conflicts and run "git commit")
#
# Unmerged paths:
#   (use "git add ..." to mark resolution)
#
# both modified:      README.md
#
no changes added to commit (use "git add" and/or "git commit -a")
```

###### Assumptions:

* I am on `feature/feature1` branch
* I have conflicts on `README.md` file

### Console

1.  To check which files contain conflicts, type:

```
git status
```

2.  After opening file you can see:

```
It is my first README file
<<<<<<< HEAD
powered by admin
=======
powered by user
>>>>>>> feature/feature1
```

3.  Identify differences between HEAD and branch

```
It is my first README file  - unchanged part of code
<<<<<<< HEAD
powered by admin            - changes from remote
=======
powered by user
>>>>>>> feature1            - change from my branch
```

4.  Decide to keep (in this case combine versions)

```
It is my first README file
powered by admin and user
```

5.  Save changes
6.  To commit all changes, type:

```
git add
git commit -m "change committer credentials"
git push origin feature1
```

### SourceTree

If you see this window after pulling/merging changes from other branch, that means you`re in trouble. But don`t worry I will show you how to get rid off these kind of problems.
![](./ST-screen-shots/Conflicts1.png)

1.  First make sure you have kdiff3 installed. (https://sourceforge.net/projects/kdiff3/?source=typ_redirect)
2.  Then go to Tools > Options menu. Select "Diff" Tab and make sure you have KDiff3 selected as default merge tool.
    ![](./ST-screen-shots/Conflicts2.png)
3.  Save changes and go to `File Status` Tab
4.  Right click on conflicted file (the one with orange triangle next to it). Select "Resolve Conflicts > Launch External Merge tool".
    ![](./ST-screen-shots/Conflicts3.png)
5.  After couple of seconds Kdiff3 will open. Now it`s time to say few words about this tool. You can see three sections in there: A, B and C. In A you can see how file looked like before any changes were made. In B you can see what changes was made to that file on the branch you are currently in (in most cases you can refer to this as "Your changes"). In C you can see what changes was made to that file on branch you were pulling from (in most cases you can refer to C as "Others/Server/Remote changes"). At the bottom of the window you can see result output. And you need to decide what will be that result output. Wheter it will be your changes? In that case just click B button in the bar at the top. Or maybe you were wrong and others were right (Naahh...). Just click C in that case. Or maybe you just realized, nobody should touch that part? Click A in that case. You can also mix the final output by choosing some part of your code and some from others. Just click B and C or type text directly in the result window.
    ![](./ST-screen-shots/Conflicts4.png)

> Dont worry if you dont get it now. Art of merging and resolving conflicts is hard to understand. But I feel force is strong in you, young GIT padawan.

6.  Once you're done with merging click save and close KDiff3.
7.  All you have to do now is click "Commit"
8.  Voila! Conflict's resolved!
9.  Thank you!
